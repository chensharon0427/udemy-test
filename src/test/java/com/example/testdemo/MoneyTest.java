package com.example.testdemo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MoneyTest {
    @Test
    void testMultiplication() {
        Money five = Money.dollar(5);
        //as a Dollar, product has its own amount field(as long as it is not private)
        assertEquals(Money.dollar(10), five.times(2));
        assertEquals(Money.dollar(15), five.times(3));

        Money five2 = Money.franc(5);
        assertEquals(Money.franc(10), five2.times(2));
        assertNotEquals(Money.franc(15), five2.times(5));
    }

    @Test
    void testEquality() {
        assertEquals(Money.dollar(5), Money.dollar(5));//have to override equals method to make it right
        assertNotEquals(Money.dollar(5), Money.dollar(8));

        assertEquals(Money.franc(5), Money.franc(5));//have to override equals method to make it right
        assertNotEquals(Money.franc(5), Money.franc(8));
    }

//    @Test
//    void testMultiplicationFranc() {
//        Money five = Money.franc(5);
//
//        //as a Dollar, product has its own amount field(as long as it is not private)
//
//    }
//
//    @Test
//    void testEqualityFranc() {
//        assertEquals(Money.franc(5), Money.franc(5));//have to override equals method to make it right
//        assertNotEquals(Money.franc(5), Money.franc(8));
//    }

    @Test
    void testCurrency(){
        assertEquals("USD",Money.dollar(1).currency());
        assertEquals("CHF",Money.franc(1).currency());
    }

    @Test
    void testSimpleAddtion(){
        Money five=Money.dollar(5);
        Expression sum=five.plus(five);
        Bank bank=new Bank();
        Money reduced=bank.reduce(sum,"USD");//reduce 10 dollar
        assertEquals(Money.dollar(10),reduced);
    }
    @Test
    void testPlusReturnSum(){
        //money.plus()
        Money five=Money.dollar(5);
        Expression result=five.plus(five);
        Sum sum= (Sum) result;//<----------
        assertEquals(five,sum.augmend);
        assertEquals(five,sum.addmend);
    }

    //bank to reduce money if you withdraw money.
    //sum add up two elements
    @Test
    void testReduceSum(){
        Expression sum=new Sum(Money.dollar(3),Money.dollar(4));
        Bank bank=new Bank();
        Money reduced=bank.reduce(sum,"USD");
        assertEquals(Money.dollar(7),reduced);

    }

}