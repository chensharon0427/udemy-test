package com.example.testdemo;

public class Sum implements Expression{
    Money augmend;
    Money addmend;

    public Sum(Money augmend, Money addmend) {
        this.augmend = augmend;
        this.addmend = addmend;
    }

    @Override
    public Money reduce(Bank bank,String currency) {
        int amount = augmend.amount + addmend.amount;
        return new Money(amount, currency);
    }
}
