package com.example.testdemo;

import java.util.Objects;

public class Dollar extends Money {

    //simplify constructor, using super
    public Dollar(int amount, String currency) {
        super(amount,currency);
    }

}
