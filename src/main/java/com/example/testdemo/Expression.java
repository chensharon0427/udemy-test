package com.example.testdemo;

public interface Expression {

    Money reduce(Bank bank, String to);
}
